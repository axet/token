package token

import (
  "strconv"
  "time"
  "strings"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
  "io"
  "errors"
  "net/http"
  "github.com/dchest/uniuri@master"
  "net/url"
)

var KEY = uniuri.NewLen(32)
var COOKIE = "token"

var EXPIRE_YEAR = 1
var EXPIRE_MONTH = 0
var EXPIRE_DAY = 0
var EXPIRE_HOUR time.Duration = 0
var EXPIRE_MIN time.Duration = 0

type AuthError struct {
  Msg string
}

func (e *AuthError) Error() string {
	return e.Msg
}

type Token struct {
  data string
  Time time.Time
}

func NewToken(id int64) *Token {
  s := strconv.FormatInt(id, 10)
  return &Token{data: s, Time: time.Now()}
}

func NewTokenData(data string) *Token {
  return &Token{data: data, Time:time.Now()}
}

func (token *Token) Data() string {
  return token.data
}

func (token *Token) Int64() int64 {
  i, err := strconv.ParseInt(token.data, 10, 64)
  if err != nil {
    panic("encypted and signed key can not be wrong type: " + err.Error())
  }
  return i
}

func (token *Token) Encode() string {
  data := url.QueryEscape(token.Data())
  now := strconv.FormatInt(token.Time.Unix(), 10)
  t := data + "|" + now
	ciphertext, err := Encrypt([]byte(KEY), []byte(t))
	if err != nil {
    panic("encyption key broken: " + err.Error())
  }
  return base64.StdEncoding.EncodeToString(ciphertext)
}

func ReadToken(r *http.Request) (*Token, error) {
  cc, err := r.Cookie(COOKIE)
  if err != nil {
    return nil, err
  }
  
  id, err := DecodeToken(cc.Value)
  if err != nil {
    return nil, err
  }
  
  return id, nil
}

func WriteToken(w http.ResponseWriter, token *Token) {
  http.SetCookie(w, &http.Cookie{Name: COOKIE, Value: token.Encode()})
}

//
// service functions
//


//
// decode token from encoded "base64==" string
//
func DecodeToken(token string) (*Token, error) {
  bb, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
    return nil, &AuthError{"invalid token hex"}
  }

	result, err := Decrypt([]byte(KEY), bb)
	if err != nil {
    return nil, &AuthError{"invalid token enc"}
  }

  ss := string(result)

  s := strings.Split(ss, "|")

  i, err := strconv.ParseInt(s[1],10,0)
	if err != nil {
    return nil, &AuthError{err.Error()}
  }

  t := time.Unix(i, 0)
  t2 := t.AddDate(EXPIRE_YEAR, EXPIRE_MONTH, EXPIRE_DAY)
  t3 := t2.Add(EXPIRE_HOUR * time.Hour + EXPIRE_MIN * time.Minute)
  if t3.Before(time.Now()) {
    return nil, &AuthError{"token expired: " + t.String()}
  }

  data, err := url.QueryUnescape(s[0])
	if err != nil {
    return nil, &AuthError{err.Error()}
  }

  return &Token{data: data, Time: t}, nil
}

// encypt string function
func Encrypt(key, text []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	b := base64.StdEncoding.EncodeToString(text)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext, nil
}

// decypt string function
func Decrypt(key, text []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	if len(text) < aes.BlockSize {
		return nil, errors.New("ciphertext too short")
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	data, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return nil, err
	}
	return data, nil
}
