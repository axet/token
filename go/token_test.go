package token

import (
  "testing"
  "fmt"
)

func TestEncodeDecode(test *testing.T) {
  KEY = "12345678901234567890123456789012"

  t := NewTokenData("123|123123|123123|123123|123123|123123|123")
  fmt.Println(t.Data())
  e := t.Encode()
  fmt.Println(e)
  t2, err := DecodeToken(e)
  if err != nil {
    fmt.Println(err)
    return
  }
  fmt.Println(t2.Data())
}
