# Token

Cookie token generation library.

Library helps you generate tokens for any purpose. You can use it to store data on user side / public place. Data you holding encypted and signed by symmetric key.

To get go dependencies use godeps.sh (https://github.com/axet/home-bin/blob/mac/godeps.sh)

## Usage Go

```go
import (
  // # go get github.com/axet/godeps && godeps
  "https://github.com/axet/token@token-0.0.1/go"
)

// data we'd like to send to user side (hidden / encrypted and signed)
data := "123|123123|123123|123123|123123|123123|123"
// craate token
t := NewTokenData(data)
// encrypt, token ready to send to user side (cookie or http get requst)
// example: BzfkP9Nwo3WjKLSm8tNWxt8t9Oiyd57mn3fOL8NFx6pVHJe03lW2kq8O4w=
token := t.Encode()

// to restore data
data_from_client := DecodeToken(token)
```

### Examples Go

```go
//
// login, generate token
//
func login(w http.ResponseWriter, r *http.Request) {
    // get account id based on email and password
    akey := ...
    // create token
    t := token.NewToken(akey.IntID())
    // writes cookie to the response, next time we will get account id instead
    // of email/password pair
    WriteToken(t, w)
}

//
// remote action ('share') based on user permission
//
func share(w http.ResponseWriter, r *http.Request) {
    token, err := token.ReadToken(r)
    if err != nil {
        protocol.Encode(w, AuthError{Msg: err.Error()})
        return
    }
    // get account id
    id := token.Int64()
    akey := getAccountFromDb(id)
}

// helping functions to store key permanently in the database

package your_package

type Config struct {
  Value string
}

func init() {
  token.KEY = // load from conf.json
}

func WriteToken(c appengine.Context, t *token.Token, w http.ResponseWriter) error {
  token.WriteToken(w, t)
}

func ReadToken(c appengine.Context, r *http.Request) (*token.Token, error) {
  return token.ReadToken(r)
}
```
